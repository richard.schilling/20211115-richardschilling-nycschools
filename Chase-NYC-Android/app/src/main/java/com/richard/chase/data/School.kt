package com.richard.chase.data

import kotlinx.serialization.Serializable

@Serializable
class School (
    val dbn: String,
    val school_name: String = "",
    val borough: String = "",
    val latitude: Number = 0,
    val longitude: Number = 0,
    val primary_address_line1: String = "",
    val city: String = "",
    val zip: String = "",
    val state_code: String = "",
    val campus_name: String = "",
    val phone_number: String = "",
    val website: String = ""
)
