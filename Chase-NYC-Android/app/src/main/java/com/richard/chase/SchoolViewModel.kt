package com.richard.chase

import android.app.Application
import android.os.Environment
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.work.*
import com.richard.chase.constants.FileNames
import com.richard.chase.constants.Keys
import com.richard.chase.constants.WorkTags.Companion.TAG_DOWNLOAD_ALLSCHOOLS
import com.richard.chase.constants.WorkTags.Companion.TAG_DOWNLOAD_SINGLESCHOOL
import com.richard.chase.constants.WorkTags.Companion.TAG_SCHOOL_PARSE
import com.richard.chase.work.Downloader
import com.richard.chase.work.SchoolListParser
import java.io.File

class SchoolViewModel(application: Application) : ViewModel() {
    val urlSchoolDetail: String = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
    // "https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4"

    private val mgr = WorkManager.getInstance(application)
    private val schoolCache: File

    class SchoolViewModelFactory(private val application: Application) : ViewModelProvider.Factory {

        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return if (modelClass.isAssignableFrom(SchoolViewModel::class.java)) {
                SchoolViewModel(application) as T
            } else {
                throw IllegalArgumentException("Unknown ViewModel class")
            }
        }
    }

    internal val downloadAllSchoolsInfos: LiveData<List<WorkInfo>> = mgr.getWorkInfosByTagLiveData(TAG_DOWNLOAD_ALLSCHOOLS)
    internal val parseInfos: LiveData<List<WorkInfo>> = mgr.getWorkInfosByTagLiveData(TAG_SCHOOL_PARSE)

    init {
        schoolCache = File( "${application.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)}/${FileNames.FILE_SCHOOLS}.json")
    }

    internal fun cacheSchools(): Boolean{

        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.CONNECTED)
            .build()

        if (schoolCache.exists()){
            return false;
        }

        val downloadRequest = OneTimeWorkRequestBuilder<Downloader>()
            .addTag(TAG_DOWNLOAD_ALLSCHOOLS)
            .setInputData(downloadWorkerData())
            .setConstraints(constraints)
            .build()
        mgr.enqueue(downloadRequest)

        return true;

    }

    internal fun parseSchools(){
        if (!schoolCache.exists()){
            return;
        }

        val parseRequest = OneTimeWorkRequestBuilder<SchoolListParser>()
            .addTag(TAG_SCHOOL_PARSE)
            .setInputData(schoolsCacheSource())
            .build()
        mgr.enqueue(parseRequest)
    }

    private fun downloadWorkerData(): Data {
        val builder = Data.Builder()
        builder.putString(Keys.KEY_DESTFILE, schoolCache.canonicalPath)
        builder.putString(Keys.KEY_URL, urlSchoolDetail)
        return builder.build()
    }

    private fun schoolsCacheSource(): Data {
        val builder = Data.Builder()
        builder.putString(Keys.KEY_SOURCEFILE, schoolCache.canonicalPath)
        return builder.build()
    }

}