package com.richard.chase.data

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class PlatformInfo(
    @SerialName("platform_name")
    val platformName: String,
    @SerialName("api_level")
    val apiLevel: Int
)