package com.richard.chase.constants

class WorkTags {
    companion object{
        const val TAG_DOWNLOAD_ALLSCHOOLS = "DOWNLOAD_ALL"
        const val TAG_DOWNLOAD_SINGLESCHOOL = "DOWNLOAD_SINGLE"
        const val TAG_SCHOOL_PARSE = "SCHOOL_PARSE"
    }
}