package com.richard.chase.data

import kotlinx.serialization.Serializable

@Serializable
class SchoolSATScores (
    val dbn: String,
    val school_name: String = "",
    val num_of_sat_test_takers: Number = -1,
    val sat_critical_reading_avg_score: Number = -1,
    val sat_math_avg_score: Number = -1,
    val sat_writing_avg_score: Number = -1
)