package com.richard.chase.constants

/**
 * Commonly used file names.
 */
class FileNames {
    companion object{
        val FILE_SCHOOLS = "schools.json"
    }
}