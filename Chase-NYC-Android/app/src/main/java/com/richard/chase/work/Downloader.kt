package com.richard.chase.work

import android.content.Context
import android.widget.Toast
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.richard.chase.constants.Keys
import java.io.*
import java.net.HttpURLConnection
import java.net.URL

class Downloader(ctx : Context, params: WorkerParameters) : Worker(ctx, params) {
    val urlSchools: String = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        // https://data.cityofnewyork.us/Education/2017-DOE-High-School-Directory/s3k6-pzi2

    override fun doWork(): Result {
        inputData.getString(Keys.KEY_URL)?.let{ source ->
            inputData.getString(Keys.KEY_DESTFILE)?.let { destFile ->
                return downloadFile(source, File(destFile))
            }
        }
        return Result.failure()
    }

    fun downloadFile(url : String, outFile : File, append: Boolean = false) : Result {

        var conn: HttpURLConnection? = null
        var inStream: InputStream? = null
        var outStream: OutputStream? = null
        var finishedNormally: Boolean = false

        if (!append && outFile.exists()){
            outFile.delete()
        }

        try {
            conn = URL(url).openConnection() as HttpURLConnection
            conn.connect()

            if (conn.responseCode != HttpURLConnection.HTTP_OK) {
                return Result.failure()
            }

            inStream = conn.inputStream
            inStream?.let {inStream ->
                outStream = FileOutputStream(outFile, false)

                val data = ByteArray(4096)
                var count = 0

                while (count > -1){
                    count = inStream.read(data)
                    if (count > -1) {
                        outStream!!.write(data, 0, count)
                    }
                }
            }

            finishedNormally = true;

            return Result.success()

        } finally {
            try {
                outStream?.flush()
                outStream?.close()
                conn?.disconnect()
            } catch (ioe: IOException) {
                // ignore
            }

            if (!finishedNormally && outFile.exists()){
                    outFile.delete()
            }
        }
    }
}