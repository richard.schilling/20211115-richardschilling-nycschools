package com.richard.chase

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.Observer
import androidx.work.WorkInfo
import com.richard.chase.ui.theme.ChaseNYCTheme

class MainActivity : ComponentActivity() {

    private val viewModel: SchoolViewModel by viewModels {
        SchoolViewModel.SchoolViewModelFactory(
            application
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // only download if list and schools if they are missing.
        // each row: name
        // selecting a school animates the dropdown.
        // material design

        // Observe work status, added in onCreate()
        viewModel.downloadAllSchoolsInfos.observe(this, downloadAlLSchoolsObserver())
        viewModel.parseInfos.observe(this, parseSchoolsObserver())
        if (viewModel.cacheSchools()){
            Toast.makeText(this, "caching schools", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "schools already cached", Toast.LENGTH_SHORT).show()
            viewModel.parseSchools()
        }

        setContent {
            ChaseNYCTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Greeting("Android")
                }
            }
        }
    }

    // Define the observer function
    private fun downloadAlLSchoolsObserver(): Observer<List<WorkInfo>> {
        return Observer { workList ->

            // If there are no matcing work info, do nothing
            if (workList.isNullOrEmpty()) {
                return@Observer
            }

            // We only care about the one output status.
            // Every continuation has only one worker tagged TAG_OUTPUT
            val workInfo = workList[0]

            if (workInfo.state.isFinished){
                Toast.makeText(applicationContext, "Finished caching. Parsing.", Toast.LENGTH_SHORT).show()
                viewModel.parseSchools()
            } else {
                // show in-progress
            }
        }
    }

    private fun parseSchoolsObserver(): Observer<List<WorkInfo>> {
        return Observer { workList ->

            // If there are no matcing work info, do nothing
            if (workList.isNullOrEmpty()) {
                return@Observer
            }

            // We only care about the one output status.
            // Every continuation has only one worker tagged TAG_OUTPUT
            val workInfo = workList[0]

            if (workInfo.state.isFinished){
                Toast.makeText(applicationContext, "Finished parsing", Toast.LENGTH_SHORT).show()
            } else {
                // show in-progress parsing
            }
        }
    }

}

@Composable
fun Greeting(name: String) {
    Text(text = "Hello $name!")
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ChaseNYCTheme {
        Greeting("Android")
    }
}