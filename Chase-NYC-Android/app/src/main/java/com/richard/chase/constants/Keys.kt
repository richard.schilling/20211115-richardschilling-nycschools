package com.richard.chase.constants

/**
 * General purpose keys for key/value pairs.
 */
class Keys {
    companion object {
        val KEY_URL = "KEY_URL"
        val KEY_DESTFILE = "KEY_DESTFILE"
        val KEY_SOURCEFILE = "KEY_SOURCEFILE"
    }
}