package com.richard.chase.work

import android.content.Context
import android.util.Log
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.richard.chase.constants.Keys
import com.richard.chase.data.SchoolSATScores
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlinx.serialization.serializer

class SchoolSATParser(ctx : Context, params: WorkerParameters): Worker(ctx, params) {

    override fun doWork(): Result {
        inputData.getString(Keys.KEY_SOURCEFILE)?.let{
            // parse
            val s = SchoolSATScores("123")
            val json = Json.encodeToString(serializer<SchoolSATScores>(), s)
            Log.i("JSON", json)

            val decodedObject = Json.decodeFromString<SchoolSATScores>(json)

            return Result.success()
        }
        return Result.failure()
    }
}